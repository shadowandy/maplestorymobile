# Rebirth Flame #

## Introduction ##

![Lionheart](/assets/images/rebirthFlameLionheart.png)

Rebirth Flames are additional options that you can add to your equipments to improve your character's overall statistics. They are similar to equipment potentials.

### Extra Options ###

The Rebirth Flames grants extra options to your equipment, increasing their statistics in areas, like:

* Phy Atk
* Mag Atk
* Crit Rate
* Crit Dmg
* Phy Def
* Mag Def

For each piece of equipment:

* Up to 2 extra options can be granted
* The same 2 extra options can be granted

### Scaling Factor ###

The above option types are not added as a hard figure (e.g. Phy Atk +36) but instead scales accordingly to the factors, like:

* Max HP
* Max MP
* Exp Increase
* Drop Rate Increase
* Meso Drop Increase
* Boss Atk
* Crit Rate
* Crit Dmg

So the options will look like `<Flame Option> scales with <Scaling Factor> <Amount>` `Crit Rate scales with Boss Atk 3.28%`.

### What are considered for Scaling Factor? ###

The following items are considered:

* Equipment & Equipment Set Option (including Pets)
* Soul & Soul Set Option (e.g. `Cygnus`, `Zak`, `Pink Bean`)
* Jewel & Jewel Set Option (`Yellow`, `Red`, `Blue`, `Green`, `Purple`)
* Style & Style Set Option (e.g. `M Label`, `B Label`, ...)
* Potential Option

**Note:** These scaling factors does not include the increases from buff ticket items (e.g. chestnuts, exp tickets).

## Offensive Options to Roll ##

Below are option lines to flame for offensive stats.

Item | Option 1 | Option 2
--- | --- | ---
Weapon | Phy/Mag Atk | Crit Dmg
Secondary | Phy/Mag Atk | Crit Dmg
Shoulder | Phy Atk | Crit Rate
Helm | Crit Dmg | -
Outfit | Crit Rate | -
Gloves | Crit Dmg | Crit Rate
Cape | Mag Atk | Crit Rate
Shoes | Crit Dmg | -
Belt | Crit Rate | -

## Recommended Scaling Factors ##

Option | Scaling Factor | Epic | Unique | Mythic
--- | --- | --- | --- | --- |
Phy/Mag Atk | Max HP | - | 0.441% | -
Phy/Mag Atk | Max MP | - | - | -
Phy/Mag Atk | Boss Atk | - | - | -
Phy/Mag Atk | Crit Rate | - | 91.1% | -
Phy/Mag Atk | Crit Dmg | - | 13.56% | -
Crit Dmg | Boss Atk | - | 3.6% | -
Crit Dmg | Crit Rate | - | 6% | -
Crit Rate | Boss Atk | - | 3.6% | -
Crit Rate | Crit Dmg | - | - | -
