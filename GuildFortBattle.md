# Guild Fort Battle #

![Guild Fort Battle](/assets/images/guildFortBattleOverview.jpg)

This is a *real-time battle between guilds*. Collective effort is required for optimal results, that is, winning the race.

The objective is to completely bring down opponent's towers before them.

On the top right corner of the game, you will find `Guild Skills` that you can use for Guild Fort Battle.

![Guild Fort Battle Skills - 1 of 2](/assets/images/guildFortBattleSkills01.png)

![Guild Fort Battle Skills - 2 of 2](/assets/images/guildFortBattleSkills02.png)

Lets go through the tasks or activities required for each role.

## Roles ##

### Everybody ##

All of us belongs to the `Everybody` role.

#### At the waiting area ####

* Ensure correct equipment
* Ensure `All Cure Potion` equipped in the slot
* Ensure Soul Gauge is charged (recommended)

#### Before Battle Starts (During Race Countdown) ####

* Party Buffs
* Personal Buffs
* Change to 4th page of Guild Skills (top right hand corner of screen)
* Start Fever

#### Battle Starts ####

* Attack the towers
* When opponent is nearing/ at the 5th tower, use the `Mini Bomb` (on 4th page of Guild Skills)

### Page 1 Owner ###

### Page 2 Owner ###

### Page 3 Owner ###
